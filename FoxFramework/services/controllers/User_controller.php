<?php

class User_controller extends \Fox\FoxServiceController {

    function __construct() {
        parent::__construct();
    }

    public function getUser() {
        $users = User::getAll();
        print \Fox\Core\Penelope::arrayToJSON($users);
    }
    
    public function postLogin() {
        $u = User::getBy("name", $_POST["name"]);
        $r = ($u->getPass() == $_POST["pass"]) ? ["state"=>1] : ["state"=>0];
        print \Fox\Core\Penelope::arrayToJSON($r);
        
    }

    public function postUser() {
        $u = new User(null,$_POST["name"],$_POST["pass"]);
        $r = $u->create();
        print \Fox\Core\Penelope::arrayToJSON($r);
    }

    public function getSaludo($nombre, $apellido) {
        if (!isset($nombre) || !isset($apellido)) {
            throw new Exception('Paremetros insuficientes.');
        }
        \Fox\Core\Request::setHeader(200, "text/plain");
        echo "Hey a" . $nombre . " " . $apellido . "!";
    }

}
